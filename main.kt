import java.util.*
import java.util.stream.Collectors
import kotlin.math.min

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
fun main(args: Array<String>) {
    val input = Scanner(System.`in`)
    var actions: MutableList<Action>
    var witches: MutableList<Witch>
    // game loop
    mainLoop@ while (true) {
        actions = mutableListOf()
        witches = mutableListOf()
        val actionCount = input.nextInt() // the number of spells and recipes in play
        for (i in 0 until actionCount) {
            val actionId = input.nextInt() // the unique ID of this spell or recipe
            val actionType = input.next() // in the first league: BREW; later: CAST, OPPONENT_CAST, LEARN, BREW
            val delta0 = input.nextInt() // tier-0 ingredient change
            val delta1 = input.nextInt() // tier-1 ingredient change
            val delta2 = input.nextInt() // tier-2 ingredient change
            val delta3 = input.nextInt() // tier-3 ingredient change
            val price = input.nextInt() // the price in rupees if this is a potion
            val tomeIndex = input.nextInt() // in the first two leagues: always 0; later: the index in the tome if this is a tome spell, equal to the read-ahead tax
            val taxCount = input.nextInt() // in the first two leagues: always 0; later: the amount of taxed tier-0 ingredients you gain from learning this spell
            val castable = input.nextInt() != 0 // in the first league: always 0; later: 1 if this is a castable player spell
            val repeatable = input.nextInt() != 0 // for the first two leagues: always 0; later: 1 if this is a repeatable player spell
            actions.add(Action(actionId, actionType, delta0, delta1, delta2, delta3, price, tomeIndex, taxCount, castable, repeatable))
        }
        for (i in 0 until 2) {
            val inv0 = input.nextInt() // tier-0 ingredients in inventory
            val inv1 = input.nextInt()
            val inv2 = input.nextInt()
            val inv3 = input.nextInt()
            val score = input.nextInt() // amount of rupees
            witches.add(Witch(inv0, inv1, inv2, inv3, score))
        }

        // Write an action using println()
        // To debug: System.err.println("Debug messages...");
        val player = witches.first() // our witch must always be first

        // in the first league: BREW <id> | WAIT; later: BREW <id> | CAST <id> [<times>] | LEARN <id> | REST | WAIT
        actions.sortDescending() // highest valued potions are in the begining
        val turn = actions.stream() // try to find a castable potion starting from the most valuable ones
                .filter { witches[0].canBrew(it) && it.type == "BREW" } // remove non-castabke potions
                .collect(Collectors.toList())
                .firstOrNull() //get only the first element, or null if no potions are castable

        if (turn == null) { // no potion can be currently cast
            val closest = actions.filter { it.type == "BREW" }.map { it to witches.first().actionsToReach(it) }.minBy { it.second }!!
            val lack = witches.first().lack(closest.first)

            val ourSpells = actions.filter { it.type == "CAST" }
                    .sortedBy { it.id }

            ingredientLoop@ for (i in lack.indices.reversed()) {
                if (player.inv[i] > 0 && lack[i] == 0) {
                    continue@ingredientLoop
                }

                if (i == 0 && player.inv[0] > 4) {  // avoid overfilling inv with tier 1 elements, as they can always be cast
                                                    // might be good to try with if inv.total > 8 as well
                    break
                }

                val candidate = ourSpells[i]
                if (candidate.castable && player.canBrew(candidate)) {
                    cast(candidate)
                    continue@mainLoop
                }

            }
        }
        val rest = "REST"
        val action = turn?.let { "${turn.type} ${turn.id}" } ?: rest
        println(action)
    }
}


class Action(val id: Int,
             val type: String,
             d0: Int,
             d1: Int,
             d2: Int,
             d3: Int,
             val price: Int,
             val index: Int,
             val tax: Int,
             val castable: Boolean,
             val repeatable: Boolean) : Comparable<Action> {
    val deltas = arrayOf(0, 0, 0, 0)

    init {
        deltas[0] = d0
        deltas[1] = d1
        deltas[2] = d2
        deltas[3] = d3
    }

    override operator fun compareTo(other: Action): Int {
        return this.price.compareTo(other.price)
    }
}

class Witch(inv0: Int,
            inv1: Int,
            inv2: Int,
            inv3: Int,
            val score: Int
) {
    val inv = arrayOf(0, 0, 0, 0)

    init {
        inv[0] = inv0
        inv[1] = inv1
        inv[2] = inv2
        inv[3] = inv3
    }

    fun canBrew(action: Action): Boolean {
        if (action.type == "OPPONENT_CAST") {
            return false
        }
        return !arrPlus(inv, action.deltas).any { it < 0 }
    }

    fun actionsToReach(action: Action): Double {
        var res = 0.0
        for (i in inv.indices) {
            res += lacking(inv[i], action.deltas[i]) * (0.5 + i)
        }
        return res
    }

    fun lack(action: Action): Array<Int> {
        val res = arrayOf(0, 0, 0, 0)
        with(action) {
            for (i in res.indices) {
                res[i] = lacking(inv[i], deltas[i])
            }
        }
        return res
    }

}

fun cast(action: Action) {
    println("${action.type} ${action.id}")
}

fun lacking(inv: Int, delta: Int): Int {
    return -min(inv + delta, 0)
}

fun arrPlus(a: Array<Int>, b: Array<Int>): Array<Int> {
    if (a.size != b.size) {
        System.err.println("arrPlus:Different array sizes")
        return emptyArray<Int>()
    }
    val res = Array(a.size) { 0 }
    for (i in a.indices) {
        res[i] = a[i] + b[i]
    }
    return res
}

fun arrMinus(a: Array<Int>, b: Array<Int>): Array<Int> {
    if (a.size != b.size) {
        System.err.println("arrPlus:Different array sizes")
        return emptyArray<Int>()
    }
    val res = Array(a.size) { 0 }
    for (i in a.indices) {
        res[i] = a[i] - b[i]
    }
    return res
}